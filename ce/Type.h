/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#ifndef DTYPE

#include <stdarg.h>
#include <limits.h>

#ifndef VALIDATION_FRAMEWORK
  #include "stm32f0xx_hal.h"
#else
  #include <stdint.h>
  #include <stdio.h>
  #include <stdlib.h>
  #include <float.h>
#endif

// Take the Test in Floating Point Precision.
// #define CNNFLOAT

// Bit-Accurate Test in Floating Point representation.
// #define DBGFLOAT


#ifdef DBGFLOAT
  #define CNNFLOAT
#endif

#ifdef CNNFLOAT
  #define DTYPE float
  #define DTYPE_MAX FLT_MAX
  #define DTYPE_MIN FLT_MIN

  #define DTYPECS float
  #define DTYPECS_MAX FLT_MAX
  #define DTYPECS_MIN FLT_MIN
#else
  #define DTYPE int8_t
  #define DTYPE_MAX CHAR_MAX // int8_t
  #define DTYPE_MIN CHAR_MIN // int8_t

  #define DTYPECS int16_t
  #define DTYPECS_MAX SHRT_MAX // int16_t
  #define DTYPECS_MIN SHRT_MIN // int16_t
#endif

#endif
