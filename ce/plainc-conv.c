/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#include "plainc-conv.h"

int plainC_conv(
	DTYPE* inp,   // input data pointer
	const DTYPE* wght,  // weights of kernels data pointer
	const DTYPE* bias,  // bias values of kernels
	DTYPE* out,   // output data pointer
	const int W,  // input Width
	const int H,  // input Height
	const int C,  // input Channels
	const int Kw, // Kernel Width
	const int Kh, // Kernel Height
	const int K,  // Number of Kernels
	const int S,  // Convolution Stride
	const int P,  // Input Zero Padding
	const int SO  // Shift Out register
) {
	// Take into consideration the zero-padding for input.
	// Zero-Padding must have been applied before to input data.
	const int IW = W + (P << 1); // Calculate zero-padded input width
	const int IH = H + (P << 1); // Calculate zero-padded input height
	const int eY = IH - Kh + 1; // End Y position for convolution
	const int eX = IW - Kw + 1; // End X position for convolution
	const int OH = (IH - Kh)/S + 1;
	const int OW = (IW - Kw)/S + 1;

	// _Pragma( "loopbound min 1 max 1" )
	for (int ki = 0; ki < K; ki++ ) { // Do for every Kernel ...
		const DTYPECS k_bias = (DTYPECS)bias[ki]; // Get Kernel's Bias value
		// Do for every output position ...

		// _Pragma( "loopbound min 1 max 1" )
		for	(int iy = 0, oy = 0; iy < eY; iy += S, oy++) {

			// _Pragma( "loopbound min 1 max 1" )
			for (int ix = 0, ox = 0; ix < eX; ix += S, ox++) {
				DTYPECS sum = 0; // Initialize accumulate value.

				// _Pragma( "loopbound min 16 max 16" )
				for (int kc = 0; kc < C; kc++) { // Do for every kernel's channel
					const DTYPE* pin = &inp[kc*IW*IH + iy*IW + ix];
					const DTYPE* pwg = &wght[ki*C*Kw*Kh + kc*Kw*Kh];

					// _Pragma( "loopbound min 3 max 3" )
					for (int ky = 0; ky < Kh; ky++) {

						// _Pragma( "loopbound min 3 max 3" )
						for (int kx = 0; kx < Kw; kx++) {
							sum += (DTYPECS)pin[ky*IW + kx] * (DTYPECS)pwg[ky*Kw + kx];
						}
					}
				}
#ifndef CNNFLOAT
				out[ki*OW*OH + oy*OW + ox] = (sum + k_bias) > 0 ?
					(DTYPE)((sum + k_bias) >> SO) : 0;
#else
				out[ki*OW*OH + oy*OW + ox] = (sum + k_bias) > 0 ?
					(DTYPE)(sum + k_bias) : 0;
#endif
			}
		}
	}
	return 0;
}
