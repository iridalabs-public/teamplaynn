/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#include "Type.h"

int plainC_maxPool(
	DTYPE* inp,   // input data pointer
	DTYPE* out,   // output data pointer
	const int W,  // input Width
	const int H,  // input Height
	const int C,  // input Channels
	const int Kw, // Kernel Width
	const int Kh, // Kernel Height
	const int S,  // Stride
	const int P   // Input Zero Padding
);
