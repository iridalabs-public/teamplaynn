/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#include "plainc-transpose.h"

int plainC_transpose(
	DTYPE* inp,   // input data pointer
	DTYPE* out,   // output data pointer
	const int IW, // input width
	const int IH, // input height
	const int IC  // input channels
) {
	for (int c=0; c < IC; c++)
		for (int y=0; y < IH; y++)
			for (int x=0; x < IW; x++)
				out[c*IW*IH + x*IH + y] = inp[c*IW*IH + y*IW + x];
	return 0;
}
