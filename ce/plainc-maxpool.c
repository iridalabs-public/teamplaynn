/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#include "plainc-maxpool.h"

int plainC_maxPool(
	DTYPE* inp,   // input data pointer
	DTYPE* out,   // output data pointer
	const int W,  // input Width
	const int H,  // input Height
	const int C,  // input Channels
	const int Kw, // Kernel Width
	const int Kh, // Kernel Height
	const int S,  // Stride
	const int P   // Input Zero Padding
) {
	// Take into consideration the zero-padding for input.
	// Zero-Padding must have been applied before to input data.
	const int IW = W + (P << 1); // Calculate zero-padded input width
	const int IH = H + (P << 1); // Calculate zero-padded input height
	const int eX = IW - Kw + 1; // End X position for maxPool
	const int eY = IH - Kh + 1; // End Y position for maxPool
	const int OW = (IW - Kw + S - 1) / S + 1; // ceil(float(IW-Kw)/float(S))+1
	const int OH = (IH - Kh + S - 1) / S + 1; // ceil(float(IH-Kh)/float(S))+1

	for (int kc = 0; kc < C; kc++) { // Do for every channel
		int iy, oy;
		for	(iy = 0, oy = 0; iy < eY; iy += S, oy++) {
			int ix, ox;
			for (ix = 0, ox = 0; ix < eX; ix += S, ox++) {
				DTYPE max = DTYPE_MIN;
				DTYPE* pin = &inp[kc*IW*IH + iy*IW + ix];
				for (int ky = 0; ky < Kh; ky++)
					for (int kx = 0; kx < Kw; kx++) {
						const DTYPE ld = pin[ky*IW + kx];
						max = (ld > max)? ld : max;
					}
				out[kc*OW*OH + oy*OW + ox] = max;
			}
			if (ox < OW) { // Handle Right Side with less Kernels size (kw instead of Kw)
				const int kw = Kw - (ix-eX) - 1;
				DTYPE max = DTYPE_MIN;
				DTYPE* pin = &inp[kc*IW*IH + iy*IW + ix];
				for (int ky = 0; ky < Kh; ky++)
					for (int kx = 0; kx < kw; kx++) {
						const DTYPE ld = pin[ky*IW + kx];
						max = (ld > max)? ld : max;
					}
				out[kc*OW*OH + oy*OW + ox] = max;
			}
		}
		if (oy < OH) { // Handle Bottom Side with less Kernels size (kh instead of Kh)
			const int kh = Kh - (iy-eY) - 1;
			int ix, ox;
			for (ix = 0, ox = 0; ix < eX; ix += S, ox++) {
				DTYPE max = DTYPE_MIN;
				DTYPE* pin = &inp[kc*IW*IH + iy*IW + ix];
				for (int ky = 0; ky < kh; ky++)
					for (int kx = 0; kx < Kw; kx++) {
						const DTYPE ld = pin[ky*IW + kx];
						max = (ld > max)? ld : max;
					}
				out[kc*OW*OH + oy*OW + ox] = max;
			}
			if (ox < OW) { // Handle Right Side with less Kernels size (kw instead of Kw)
				const int kw = Kw - (ix-eX) - 1;
				DTYPE max = DTYPE_MIN;
				DTYPE* pin = &inp[kc*IW*IH + iy*IW + ix];
				for (int ky = 0; ky < kh; ky++)
					for (int kx = 0; kx < kw; kx++) {
						const DTYPE ld = pin[ky*IW + kx];
						max = (ld > max)? ld : max;
					}
				out[kc*OW*OH + oy*OW + ox] = max;
			}
		}
	}
	return 0;
}
