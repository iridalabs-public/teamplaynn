/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#ifndef __COMPUTE_ELEMENTS_HEADER_FILE__
#define __COMPUTE_ELEMENTS_HEADER_FILE__
#include "Type.h"
#include "plainc-conv.h"
#include "plainc-fc.h"
#include "plainc-maxpool.h"
#include "plainc-transpose.h"
#endif
