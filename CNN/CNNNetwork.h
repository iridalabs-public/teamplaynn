/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#ifndef CNNNETWORK_HEADER
#define CNNNETWORK_HEADER
#include "../ce/ce.h"



DTYPE* getInpBuffer();

void PreProcessRGBImage(
	uint8_t* data,
	int W,
	int H
);

int CNNNetwork(
	int* result  // An integer showing the classification result
);

#endif
