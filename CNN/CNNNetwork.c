/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#include "CNNNetwork.h"

#include "Model/Coeffs.h"


static DTYPE MemBuffA[3072];
static DTYPE MemBuffB[6144];







/**
 * @brief This function provides an access point to the input memory address
 * for the CNN. In that way user can fill this memory with preprocessed data.
 * The function is usually used for testing/debugging issues.
 *
 * @return DTYPE* Pointer to the memory address which is going to be used as
 * input from the CNNNetwork() function.
 */
DTYPE* getInpBuffer() {
	return MemBuffA;
}








/**
 * @brief Preprocessing uint8_t input image data. This preprocessing contain the
 * following actions: 1) Convert RGB to BGR, 2) Transpose image, 3) Convert to
 * DTYPE type. 4) Remove a predefined value from all pixels for each channel.
 *
 * @param data Pointer to the data memory address
 * @param W Width of the image
 * @param H Height of the image.
 */
void PreProcessRGBImage(
	uint8_t* data,
	int W,
	int H
) {
  const int32_t mean_data[3]   = {125,125,125};

	for (int c = 0; c < 3; c++) {
		uint8_t* idata_ = &data[c*32*32];       // Read from RGB
		DTYPE* odata_ = &MemBuffA[(2-c)*32*32]; // Write to BGR
		for (int y = 0; y < H; y++)
			for (int x = 0; x < W; x++) {
				odata_[x*H+y] = (DTYPE)( ((int32_t)idata_[y*W+x])-mean_data[c] );
			}
	}
}









/**
 * @brief Implementation of the CNN model utilizing the provided compute
 * elements.
 *
 * @param result The single class ID that the image was classified.
 * @return int Error number. If it's zero, then the function has completed
 * succesfully.
 */
int CNNNetwork(
	int* result
) {
	int err = 0;

	// conv1 + relu1
	err = plainC_conv(
		MemBuffA,
		conv1_w,
		conv1_b,
		MemBuffB,
		32, 32, 3,
		1, 1, 6,
		1, 0,
		7
	);
	if (err) return err;

	// pool1
	err = plainC_maxPool(
		MemBuffB,
		MemBuffA,
		32, 32, 6,
		2, 2,
		2, 0
	);
	if (err) return err;

	// conv2 + relu2
	err = plainC_conv(
		MemBuffA,
		conv2_w,
		conv2_b,
		MemBuffB,
		16, 16, 6,
		1, 1, 16,
		1, 0,
		5
	);
	if (err) return err;

	// pool2
	err = plainC_maxPool(
		MemBuffB,
		MemBuffA,
		16, 16, 16,
		2, 2,
		2, 0
	);
	if (err) return err;

	// fc1
	err = plainC_fc(
		MemBuffA,
		fc1_w,
		fc1_b,
		MemBuffB,
		1,
		8*8*16,
		2,
		6
	);
	if (err) return err;

	*result = (MemBuffB[0] > MemBuffB[1]) ? 0 : 1;

	return err;
}
