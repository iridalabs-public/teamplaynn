# -----------------------------------------------------------------
# Copyright (c) 2008-2020 IRIDA LABS.
# All Rights Reserved IRIDA LABS Proprietary
#
# All ideas, data and information contained in or disclosed by this document
# are confidential and proprietary information of IRIDA LABS and all rights
# therein are expressly reserved. By accepting this material the recipient
# agrees that this material and the information contained therein are held in
# confidence and in trust and will not be used, copied, reproduced in whole or
# in part, nor its contents revealed in any manner to others without the
# express written permission of IRIDA LABS.
# -------------------------------------------------------------------
import os
from os.path import isfile
import sys
import cv2
import io
from subprocess import Popen
import subprocess
import numpy as np
import math
import threading
import re
import matplotlib.pyplot as plt
import datetime
import struct
import scipy.misc
from PIL import Image
from skimage import data
from skimage.transform import resize

HeaderFile = "../Databases/Val50Images.h"

FileFolder = os.path.dirname(os.path.realpath(__file__))+"\\"
FileFolder = FileFolder.replace("\\","/")
hfiledata = [line.rstrip('\n') for line in open(FileFolder+HeaderFile, 'r')]

ImageFolder = FileFolder + HeaderFile.replace(".h","/")
print("ImageFolder: %s" % (ImageFolder))
if not os.path.exists(ImageFolder):
  os.makedirs(ImageFolder)

cnt = 0
for row in hfiledata:
  if (row[0:2] != "  "):
    continue
  data = list(map(int, row[3:-2].split(',')))
  data = np.asarray(data)
  data = np.uint8(data)
  data = data.reshape(3,32,32)
  data = np.transpose(data,(2,1,0))
  imgpath = ImageFolder+"Image_%4.4i.jpg" % (cnt)
  img = Image.fromarray(data)
  img.save(imgpath)
  cnt = cnt + 1

print("Done.")


