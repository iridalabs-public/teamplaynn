/** -----------------------------------------------------------------
Copyright (c) 2008-2020 IRIDA LABS.
All Rights Reserved IRIDA LABS Proprietary

All ideas, data and information contained in or disclosed by this document
are confidential and proprietary information of IRIDA LABS and all rights
therein are expressly reserved. By accepting this material the recipient
agrees that this material and the information contained therein are held in
confidence and in trust and will not be used, copied, reproduced in whole or
in part, nor its contents revealed in any manner to others without the
express written permission of IRIDA LABS.
------------------------------------------------------------------- **/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif


#include "../CNN/CNNNetwork.h"

void Error_Handler(void);

#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA

#ifdef __cplusplus
}
#endif

#endif
